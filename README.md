Jupyter notebook to make Olivier Laprévote's Master thesis report. 
Thesis title: *Relationships between physical values quantifying the
energy transmission between residues in protein*

# Installation

To clone this repository on your computer, simply run:
```
git clone https://gitlab.com/OLaprevote/inter-residue-thermal-conductivity-report.git
```

Python libraries are specified in `environment.yml`.
If you use conda you can set up a conda environment named `master_report`
using this yamel file by simply running:
```
conda env create --name master_report -f environment.yml
```
Then to activate this environment:
```
conda activate master_report
```
Finally you can read `report_nb.ipynb` by launching jupyter notebook 
in the repository typing `jupyter notebook`.

More useful commands from [conda documentation](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html):
 - Update dependencies from the environment: 
    `conda env update -n master_report -f environment.yml`
 - Forgetting how you called the environment so you need to list all of them:
    `conda env list`

`Jupyter` also needs extensions to fully render like an article and convert it in pdf:
 - cite2c for the citations. To fully install it you should run `python3 -m cite2c.install` after the first environment activation
 - jupyter contrib nbextension. It enables code folding and equation numbering. To fully install it run in the activated environment:
```
jupyter contrib nbextension install
jupyter nbextension enable codefolding/main
jupyter nbextension enable equation-numbering/main
```

# Convert to thesis
The thesis had to have text on recto, and figures and tables on verso,
facing the text. This is automatically done during pdf conversion.
To convert the jupyter notebook in pdf, simply type in a terminal:
```
jupyter nbconvert --to=pdf report_nb.ipynb
```
This permits a quick preview, however you will see that a few things are
lacking. The figures are all cramped, the references don't show and the tables
are not named.
To render the references, you will need to convert in latex first, then run
`pdflatex` and `bibtex`:
```
jupyter nbconvert --to=pdf report_nb.ipynb
pdflatex report_nb.tex
bibtex report_nb
pdflatex report_nb.tex
pdflatex report_nb.tex
```
Figures are still cramped and tables are still not named, though. Sadly I
didn't take the time to find a work around, so you need to go in the latex
file and move and name everything, knowing that figures appear on the white
page after the one they are called.
This is done in the file `report_tex.tex`

The thesis also had to figure the abstract on the last page, and a bunch
of information had to be on the title page, but I was lazy and didn't want
to bother with doing all this in latex, so I did both appart on libreoffice,
exported them in pdf, and merged everything.

# Credits
I am no jupyter genius and it was my first time seriously using latex, so
here are the solutions I found online to solve my main problems:
- [Making publication ready jupyter notebook][notebook-article] from Julius
Schulz.  You can find here all about puting references and rendering nice
figures with captions in the pdf. I modified a bit the `bibpreprocessor.py`
as it considered everything as an article, and I had books and others. 
- "*Making displayed pandas table render like latex tables when exported*"
[subject on Stack Overflow][pandas-latex], first answer.
- "*Figures on left pages, text on right ones with class book, numbering only 
on right pages*" [subject on TeX Stack Exchange][latex-fig-verso], first answer.


[latex-fig-verso]: https://tex.stackexchange.com/questions/18156/figures-on-left-pages-text-on-right-ones-with-class-book-numbering-only-on-rig
[notebook-article]: http://blog.juliusschulz.de/blog/ultimate-ipython-notebook
[pandas-latex]:https://stackoverflow.com/questions/20685635/pandas-dataframe-as-latex-or-html-table-nbconvert
